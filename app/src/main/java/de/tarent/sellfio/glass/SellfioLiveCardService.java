package de.tarent.sellfio.glass;

import com.google.android.glass.timeline.LiveCard;
import com.google.android.glass.timeline.LiveCard.PublishMode;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * A {@link Service} that publishes a {@link LiveCard} in the timeline.
 */
public class SellfioLiveCardService extends Service implements SellfioListener {

    private static final String LIVE_CARD_TAG = "SellfioLiveCardService";
    public static final String ACTION_DISPLAY = "de.tarent.sellfio.glass.DISPLAY";

    private LiveCard mLiveCard;
    private RemoteViews mRemoteViews;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mLiveCard == null) {
            mLiveCard = new LiveCard(this, LIVE_CARD_TAG);

            mRemoteViews = new RemoteViews(getPackageName(), R.layout.sellfio_live_card);
            mLiveCard.setViews(mRemoteViews);

            Intent actionIntent = new Intent(this, ScanActivity.class);
            mLiveCard.setAction(PendingIntent.getActivity(this, 0, actionIntent, 0));
            mLiveCard.publish(PublishMode.REVEAL);

            SellfioBluetoothManager.getInstance().addListener(this);
            SellfioBluetoothManager.getInstance().connect();
        } else {
            mLiveCard.navigate();
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mLiveCard != null && mLiveCard.isPublished()) {
            mLiveCard.unpublish();
            mLiveCard = null;
        }
        super.onDestroy();
    }

    @Override
    public void onSellfioMessage(String message) {

    }
}
