package de.tarent.sellfio.glass;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class SellfioBluetoothManager {
    public static final String BLUETOOTH_SERVICE_UUID = "643daa50-1721-46e1-8e9f-88bd26860a9b";

    private static final SellfioBluetoothManager instance = new SellfioBluetoothManager();
    private BluetoothAdapter bluetoothAdapter;

    private DataInputStream inputStream;
    private DataOutputStream outputStream;
    private BluetoothSocket socket;

    private ArrayList<SellfioListener> listeners = new ArrayList<SellfioListener>();

    private Object lock = new Object();

    public static SellfioBluetoothManager getInstance(){
        return instance;
    }

    private SellfioBluetoothManager(){

    }

    private boolean checkForBluetooth(){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            return true;
        }

        bluetoothAdapter = null;
        return false;
    }

    public boolean connect() {
        socket = null;
        if(socket != null){
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (checkForBluetooth() && (socket == null || !socket.isConnected())) {
            Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) {
                    try {
                        socket = device.createRfcommSocketToServiceRecord(UUID.fromString(BLUETOOTH_SERVICE_UUID));
                        socket.connect();

                        if(socket.isConnected()){
                            inputStream = new DataInputStream(socket.getInputStream());
                            outputStream = new DataOutputStream(socket.getOutputStream());

                            Thread thread = new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    while(socket != null && socket.isConnected()){
                                        try {
                                            String str = inputStream.readUTF();
                                            synchronized (this) {
                                                for (SellfioListener listener : listeners) {
                                                    listener.onSellfioMessage(str);
                                                }
                                            }
                                            Log.d(getClass().getSimpleName(), "recv: " + str);
                                        } catch (IOException e) {
                                            socket = null;
                                            Log.e(getClass().getSimpleName(), e.getMessage());
                                        }
                                    }
                                }
                            });

                            thread.start();

                            sendMessage("handshake " + new Date().toString());

                            //only handling one device at the moment
                            return true;
                        }
                    } catch (IOException e) {
                        Log.e(getClass().getSimpleName(), e.getMessage());
                    }
                }
            }
        }

        return false;
    }

    public void addListener(SellfioListener listener){
        synchronized (this) {
            listeners.add(listener);
        }
    }

    public void removeListener(SellfioListener listener){
        synchronized (this) {
            listeners.remove(listener);
        }
    }

    public void sendMessage(String message){
        synchronized (lock) {
            try {
                boolean send = true;

                if(!isConnected()){
                    send = connect();
                }

                if(send) {
                    outputStream.writeUTF(message);
                    Log.d(getClass().getSimpleName(), "send: " + message);
                }
            } catch (IOException e) {
                Log.e(getClass().getSimpleName(), e.getMessage());
            }
        }
    }

    private boolean isConnected(){
        return socket != null && socket.isConnected();
    }
}
