package de.tarent.sellfio.glass;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;

public class ScanActivity extends Activity {
    public static final int REQUESTCODE_SCAN = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            startActivityForResult(new Intent("com.github.barcodeeye.SCAN"), REQUESTCODE_SCAN);
        } catch (ActivityNotFoundException e) {
            // display error message that no barcode scanner is installed
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        openOptionsMenu();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUESTCODE_SCAN) {
            if (resultCode == RESULT_OK) {
                String barcode = data.getStringExtra("SCAN_RESULT");
                Intent broadcast = new Intent(SellfioLiveCardService.ACTION_DISPLAY);
                broadcast.putExtra("text", barcode);
                sendBroadcast(broadcast);
                SellfioBluetoothManager.getInstance().sendMessage("barcode " + barcode);
                finish();
            }
        }
    }
}
