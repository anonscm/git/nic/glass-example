package de.tarent.sellfio.glass;

public interface SellfioListener {
    public void onSellfioMessage(String message);
}
